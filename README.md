## OpenIA Chatbot Script

This is a simple bash script `op.sh` that interacts with the OpenAI API to create a chatbot. The script uses `jq` for JSON manipulation and `curl` for making API requests.

### Requirements

- Bash
- `jq` for JSON processing
- `curl` for API requests
- OpenAI API key stored in a file located at `~/.op/API_KEY`

### Usage

Run the script with the following command:

```bash
./op.sh [OPTIONS] [TEXT]
```

#### OPTIONS:
- `--help`: Show this help message.

#### TEXT:
- Text input for the OpenAI chatbot.
- If no text is provided as an argument, the script will prompt for input.

### Setup

1. Ensure you have the necessary tools (`bash`, `jq`, `curl`) installed on your system.
2. Save your OpenAI API key in a file located at `~/.op/API_KEY`:
   ```bash
   echo "YOUR_API_KEY" > ~/.op/API_KEY
   ```

3. Give execute permission to the script:
   ```bash
   chmod +x op.sh
   ```

### Detailed Explanation

- The script starts by defining the working directory and a help function.
- It checks for the presence of the API key file (`~/.op/API_KEY`).
- It creates a directory for conversation logs (`~/.op/conv`) and generates a JSON file for each session based on the current timestamp.
- It reads user input either from arguments or interactively.
  - If no text is input via arguments, it prompts the user to enter input interactively.
  - It supports reading file contents and evaluating bash commands within the input.
- The input text is converted to JSON format and sent to the OpenAI API.
- The API response is processed and displayed in the terminal.

### Example

To start an interactive session:
```bash
./op.sh
```

To send a direct message:
```bash
./op.sh "Hello, how are you?"
```
